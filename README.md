<div id="top"></div>
<!-- INTRO -->
<div align="center">
<h3 align="center">Drone.cpp</h3>

  <p align="center">
    Project Drone.cpp was created to export set of MAVSDK C++ operations so they can be imported in C# client application.
  </p>
</div>


<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
  </ol>
</details>


<!-- ABOUT THE PROJECT -->
## About The Project
Project was created as Proof of Concept. It exports set of MAVSDK C++ operations as functions using C linkage so the compiler does not mangle their names. Exported functions can be used in Drone.cs project to control any MAVLink compatible drone.
<p align="right">(<a href="#top">back to top</a>)</p>

### Built With

* [CMake](https://cmake.org)
* [GCC/G++](https://gcc.gnu.org)
* [Doxygen](https://doxygen.nl)

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- GETTING STARTED -->
## Getting Started
To use the software you need to perform following steps on Debian based system.

### Prerequisites
* Install MAVSDK C++ required components
  ```sh
  sudo apt-get install --yes cmake build-essential colordiff git doxygen graphviz
  ```

### Installation
* Clone the repository
  ```sh
  git clone https://gitlab.com/pAULSG1/drone.cpp
  ```
  * Prepare repository
  ```sh
  cd MAVSDK
  git submodule update --init --recursive
  ```
* Build either Debug or Release MAVSDK C++ libraries
  ```sh
  export MAKEFLAGS=-j$(grep -c ^processor /proc/cpuinfo)
  cmake -DCMAKE_BUILD_TYPE=Debug -DBUILD_SHARED_LIBS=ON -Bbuild/default -H.
  #cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=ON -Bbuild/default -H.
  cmake --build build/default --target install
  ```
* Verify MAVSDK C++ build by running building examples
  ```sh
  sed -i 's/Tune::result_str(result)/result/' examples/tune/tune.cpp # fix out of date example
  for i in `ls examples/ | grep -v CMakeLists.txt`; do
    cd examples/;
    mkdir -p "$i/build";
    cd "$i/build";
    cmake -DCMAKE_RULE_MESSAGES:BOOL=ON -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON .. --verbose;
    make VERBOSE=1;
    cd -
  done
  ```
* Build library and executable
  ```sh
  mkdir build
  cmake ..
  make
  ```
* Install library
  ```sh
  sudo make install
  ```

<p align="right">(<a href="#top">back to top</a>)</p>

## Usage
You can use library to control your MAVLink compatible drone or test us using executable build with library from the same source. Library is also compatible with Gazebo simulated PX4 based drones.
For more usage and integration with other languages examples, please refer to https://gitlab.com/pAULSG1/drone.cs.


<p align="right">(<a href="#top">back to top</a>)</p>
