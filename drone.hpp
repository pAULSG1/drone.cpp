#include <chrono>
#include <cstdint>
#include <mavsdk/mavsdk.h>
#include <mavsdk/plugins/action/action.h>
#include <mavsdk/plugins/mission/mission.h>
#include <mavsdk/plugins/telemetry/telemetry.h>
#include <mavsdk/plugins/camera/camera.h>
#include <mavsdk/plugins/gimbal/gimbal.h>
#include <iostream>
#include <thread>
#include <unistd.h>
#include <functional>
#include <future>
#include <memory>


using namespace mavsdk;

/**
 * @brief Statistics provided by module
 * 
 */
class statistics {
    public:
    /**
     * @brief Latitude in degrees
     * 
     */
    double latitude_deg;
    /**
     * @brief Longitude in degrees
     * 
     */
    double longitude_deg;
    /**
     * @brief Absolute altitude in meters
     * 
     */
    float absolute_altitude_m;
    /**
     * @brief Relative altitude in meters (relative to take off point)
     * 
     */
    float relative_altitude_m;
    /**
     * @brief Maximal speed set up
     * 
     */
    float max_speed;
    /**
     * @brief Default takeoff altitude
     * 
     */
    float takeoff_altitude;
    /**
     * @brief Default return to take off point altitude
     * 
     */
    float return_to_launch_return_altitude;
    /**
     * @brief Remaining battery's power in percents
     * 
     */
    float battery_remaining_percent;
    /**
     * @brief Battery's voltage
     * 
     */
    float battery_voltage;
};
/**
 * @brief Takes off
 * Makes drone take off and begin to hover over area
 */
extern "C" Action::Result takeoff();
/**
 * @brief Lands
 * Makes drone land on the ground
 */
extern "C" Action::Result land();
/**
 * @brief Spins the motors.
 * Drone needs to be armed before taking off
 * 
 */
extern "C" Action::Result arm();
/**
 * @brief Stops the motors.
 * Drone needs to be landed before disarming
 * 
 */
extern "C" Action::Result disarm();
/**
 * @brief Provided set of statistics related to drone
 * 
 */
extern "C" statistics* get_stats();
/**
 * @brief Sets camera's gimbal pitch and yaw
 * 
 * @param [in] pitch_deg Gimbal's pitch in degrees
 * @param [in] yaw_deg Gimbal's yaw in degrees
 */
extern "C" Gimbal::Result set_pitch_and_yaw(float pitch_deg, float yaw_deg);
/**
 * @brief Flies drone to provided destination
 * 
 * @param [in] latitude_deg Destination's latitude in degrees
 * @param [in] longitude_deg Destination's longitude in degrees
 * @param [in] absolute_altitude_m Destination's absolute altitude in meters
 * @param [in] yaw_deg Drone's yaw in degrees
 */
extern "C" Action::Result goto_position(double latitude_deg, double longitude_deg, float absolute_altitude_m, float yaw_deg);
/**
 * @brief Takes photo from camera
 * 
 */
extern "C" Camera::Result take_photo();
/**
 * @brief Starts recording video from camera
 * 
 */
extern "C" Camera::Result start_video();
/**
 * @brief Stops recording video from camera
 * 
 */
extern "C" Camera::Result stop_video();