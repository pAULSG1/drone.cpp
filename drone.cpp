#include "drone.hpp"

using namespace std::this_thread;
using namespace std::chrono;

/**
 * @brief Turns text on console red
 * 
 */
#define ERROR_CONSOLE_TEXT "\033[31m"
/**
 * @brief Turns text on console blue
 * 
 */
#define TELEMETRY_CONSOLE_TEXT "\033[34m"
/**
 * @brief Restores normal console colour
 * 
 */
#define NORMAL_CONSOLE_TEXT "\033[0m"

/**
 * @brief Shared object representing drone connection
 * 
 */
Mavsdk *dc;
/**
 * @brief Drone's telemetry access
 * 
 */
std::shared_ptr<Telemetry> telemetry;
/**
 * @brief Drone's action capabilities
 * 
 */
std::shared_ptr<Action> action;
/**
 * @brief Drone's mission access
 * 
 */
std::shared_ptr<Mission> mission;

/**
 * @brief Drone's camera access
 * 
 */
std::shared_ptr<Camera> camera;
/**
 * @brief Drone's gimbal access
 * 
 */
std::shared_ptr<Gimbal> gimbal;

/**
 * @brief Display information about discovered drone
 * 
 * @param component_type Type of detected device
 */
void component_discovered(ComponentType component_type);
/**
 * @brief Function verifying whether provided value is NaN
 * This function verifies whether provided value is NaN so it can be handled and replaced by special value
 * 
 * @param [in] var Provided value
 * @return true Returns true if provided value is NaN
 * @return false Returns false if provided value is not NaN
 */
bool custom_isnan(double var);
/**
 * @brief Operation not longer used, left for backward compatibility
 * 
 * @return Action::Result 
 */
Action::Result connect()
{
    return Action::Result::Success;
}

extern "C" Action::Result arm()
{
    // Check if vehicle is ready to arm
    //while (telemetry->health_all_ok() != true) {
    //    std::cout << "Vehicle is getting ready to arm" << std::endl;
    //    sleep_for(seconds(1));
    //}
    
    std::cout << "Arming..." << std::endl;
    const Action::Result arm_result = action->arm();

    if (arm_result != Action::Result::Success) {
        std::cout << ERROR_CONSOLE_TEXT << "Arming failed:" << arm_result << NORMAL_CONSOLE_TEXT << std::endl;
    }
    return arm_result;
}

extern "C" Action::Result disarm()
{
    std::cout << "Disarming..." << std::endl;
    const Action::Result disarm_result = action->disarm();

    if (disarm_result != Action::Result::Success) {
        std::cout << ERROR_CONSOLE_TEXT << "Disarming failed:" << disarm_result << NORMAL_CONSOLE_TEXT << std::endl;
    }
    return disarm_result;
}

extern "C" Action::Result takeoff()
{
    std::cout << "Taking off..." << std::endl;
    const Action::Result takeoff_result = action->takeoff();
    if (takeoff_result != Action::Result::Success) {
        std::cout << ERROR_CONSOLE_TEXT << "Takeoff failed:" << takeoff_result << NORMAL_CONSOLE_TEXT << std::endl;
        return takeoff_result;
    }
	return Action::Result::Success;
}

extern "C" Gimbal::Result set_pitch_and_yaw(float pitch_deg, float yaw_deg)
{
	Gimbal::Result gimbal_result = gimbal->set_pitch_and_yaw(pitch_deg, yaw_deg);
	if (gimbal_result != Gimbal::Result::Success) {
        std::cout << ERROR_CONSOLE_TEXT << "Take photo failed:" << gimbal_result << NORMAL_CONSOLE_TEXT << std::endl;
        return gimbal_result;
    }
    return Gimbal::Result::Success;
}

extern "C" Action::Result land()
{
    std::cout << "Landing..." << std::endl;
    const Action::Result land_result = action->land();
    if (land_result != Action::Result::Success) {
        std::cout << ERROR_CONSOLE_TEXT << "Land failed:" << land_result << NORMAL_CONSOLE_TEXT << std::endl;
        return land_result;
    }

    // Check if vehicle is still in air
    while (telemetry->in_air()) {
        std::cout << "Vehicle is landing..." << std::endl;
        sleep_for(seconds(1));
    }
    std::cout << "Landed!" << std::endl;
    return Action::Result::Success;
}

extern "C" Action::Result goto_position(double latitude_deg, double longitude_deg, float absolute_altitude_m, float yaw_deg)
{
    std::cout << "Going to..." << std::endl;
    const Action::Result goto_result = action->goto_location(latitude_deg, longitude_deg, absolute_altitude_m, yaw_deg);
    if (goto_result != Action::Result::Success) {
        std::cout << ERROR_CONSOLE_TEXT << "Going to failed:" << goto_result << NORMAL_CONSOLE_TEXT << std::endl;
    }
    return goto_result;
}

extern "C" Camera::Result take_photo()
{
    std::cout << "Setting camera mode" << std::endl;
    const Camera::Result mode_result = camera->set_mode(Camera::Mode::Photo);

    if (mode_result != Camera::Result::Success) {
        std::cout << ERROR_CONSOLE_TEXT << "Set mode failed:" << mode_result << NORMAL_CONSOLE_TEXT << std::endl;
        return mode_result;
    }
    sleep(1);

    std::cout << "Taking photo" << std::endl;
    const Camera::Result camera_result = camera->take_photo();
    if (camera_result != Camera::Result::Success) {
        std::cout << ERROR_CONSOLE_TEXT << "Take photo failed:" << camera_result << NORMAL_CONSOLE_TEXT << std::endl;
        return camera_result;
    }
    
    return Camera::Result::Success;
}

extern "C" Camera::Result start_video()
{
    std::cout << "Setting camera mode" << std::endl;
    const Camera::Result mode_result = camera->set_mode(Camera::Mode::Video);

    if (mode_result != Camera::Result::Success) {
        std::cout << ERROR_CONSOLE_TEXT << "Set mode failed:" << mode_result << NORMAL_CONSOLE_TEXT << std::endl;
        return mode_result;
    }
    sleep(1);

    std::cout << "Starting video" << std::endl;
    const Camera::Result camera_result = camera->start_video();

    if (camera_result != Camera::Result::Success) {
        std::cout << ERROR_CONSOLE_TEXT << "Start video failed:" << camera_result << NORMAL_CONSOLE_TEXT << std::endl;
        return camera_result;
    }
    return Camera::Result::Success;
}

extern "C" Camera::Result stop_video()
{
    std::cout << "Stopping video" << std::endl;
    const Camera::Result camera_result = camera->stop_video();

    if (camera_result != Camera::Result::Success) {
        std::cout << ERROR_CONSOLE_TEXT << "Stop video failed:" << camera_result << NORMAL_CONSOLE_TEXT << std::endl;
        return camera_result;
    }
    return Camera::Result::Success;
}

extern "C" statistics* getstats()
{
    statistics* stats = new statistics();
    auto data = action->get_takeoff_altitude ();
    if (data.first != Action::Result::Success) {
        std::cout << ERROR_CONSOLE_TEXT << "Arming failed:" << data.first << NORMAL_CONSOLE_TEXT << std::endl;
    }
    else {
		std::cout << "takeoff altitude: " << data.second << std::endl;
	}
    stats->takeoff_altitude = data.second == data.second ? data.second : -11111;

    data = action->get_maximum_speed ();
    if (data.first != Action::Result::Success) {
        std::cout << ERROR_CONSOLE_TEXT << "get_maximum_speed failed:" << data.first << NORMAL_CONSOLE_TEXT << std::endl;
    }
    else {
		std::cout << "max_speed: " << data.second << std::endl;
	}
    stats->max_speed = data.second == data.second ? data.second : -11111;

    data = action->get_return_to_launch_altitude ();
    if (data.first != Action::Result::Success) {
        std::cout << ERROR_CONSOLE_TEXT << "get_return_to_launch_altitude:" << data.first << NORMAL_CONSOLE_TEXT << std::endl;
    }
    else {
		std::cout << "return_to_launch_altitude: " << data.second << std::endl;
	}
    stats->return_to_launch_return_altitude = data.second == data.second ? data.second : -11111;

    std::cout << TELEMETRY_CONSOLE_TEXT;
    auto position = telemetry->position();
	std::cout << "latitude_deg: " << position.latitude_deg << std::endl;
    stats->latitude_deg = position.latitude_deg == position.latitude_deg ? position.latitude_deg : -11111;
	std::cout << "longitude_deg: " << position.longitude_deg << std::endl;
    stats->longitude_deg = position.longitude_deg == position.longitude_deg ? position.longitude_deg : -11111;
	std::cout << "absolute_altitude_m: " << position.absolute_altitude_m << std::endl;
    stats->absolute_altitude_m = position.absolute_altitude_m == position.absolute_altitude_m ? position.absolute_altitude_m : -11111;
	std::cout << "relative_altitude_m: " << position.relative_altitude_m << std::endl;
    stats->relative_altitude_m = position.relative_altitude_m == position.relative_altitude_m ? position.relative_altitude_m : -11111;

    auto battery = telemetry->battery();
    std::cout << "voltage_v: " << battery.voltage_v << std::endl;
    stats->battery_voltage = battery.voltage_v == battery.voltage_v ? battery.voltage_v: -11111;
    std::cout << "remaining_percent: " << battery.remaining_percent << std::endl;
    stats->battery_remaining_percent = battery.remaining_percent == battery.remaining_percent ? battery.remaining_percent : -11111;
    
	std::cout << NORMAL_CONSOLE_TEXT << std::endl;
    
    return stats;
}

/**
 * @brief Library's/Program's constructors
 * Sets up environment and set's up communication with the drone
 * If no drone is discovered, library/program should exit
 */
void __attribute__ ((constructor)) initLibrary(void) {
    dc = new Mavsdk();
    std::string connection_url;
    ConnectionResult connection_result;

    bool discovered_system = false;
    connection_url = "udp://:14540";
    connection_result = dc->add_any_connection(connection_url);

    if (connection_result != ConnectionResult::Success) {
        std::cout << ERROR_CONSOLE_TEXT << "Connection failed: " << connection_result << NORMAL_CONSOLE_TEXT << std::endl;
        exit(1);
        return;
    }

    System& sys = dc->system();
    std::cout << "Waiting to discover system..." << std::endl;
    dc->register_on_discover([&discovered_system](uint64_t uuid) {
        std::cout << "Discovered system with UUID: " << uuid << std::endl;
        discovered_system = true;
    });

    sleep_for(seconds(2));

    if (!discovered_system) {
        std::cout << ERROR_CONSOLE_TEXT << "No system found, exiting." << NORMAL_CONSOLE_TEXT << std::endl;
        exit(1);
        return;
    }

    sys.register_component_discovered_callback(component_discovered);

    action = std::make_shared<Action>(sys);
    mission = std::make_shared<Mission>(sys);
    telemetry = std::make_shared<Telemetry>(sys);
    camera = std::make_shared<Camera>(sys);
    gimbal = std::make_shared<Gimbal>(sys);

    std::cout << "Library is initialized" << std::endl;
}

/**
 * @brief Library's'/Program's destructor
 * Cleans up leftovers
 */
void __attribute__ ((destructor)) cleanUpLibrary(void) {
    delete dc;
    std::cout << "Library is exited" << std::endl;
}

/**
 * @brief Main function used for testing library code
 * 
 * @param [in] argc Number of program arguments
 * @param [in] argv List of program arguments
 * @return int Program's return code
 */
int main(int argc, char **argv){
    (void)argc; // skip "unused parameter" and "statement has no effect" warnings/errors
    (void)argv; // skip "unused parameter" and "statement has no effect" warnings/errors
    return 0;
}


void component_discovered(ComponentType component_type)
{
    std::cout << NORMAL_CONSOLE_TEXT << "Discovered a component with type " << unsigned(component_type) << std::endl;
}

bool custom_isnan(double var)
{
    volatile double d = var;
    return d != d;
}
