cmake_minimum_required(VERSION 2.8.12)

project(drone)

set(CMAKE_VERBOSE_MAKEFILE ON) # make make full debug
set(CMAKE_RULE_MESSAGES OFF) # but no building and linking messages

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

if(NOT MSVC)
    add_definitions("-std=c++11 -Wall -Wextra -Werror")
else()
    add_definitions("-std=c++11 -WX -W2")
endif()

find_package(MAVSDK REQUIRED)

# create executable
add_executable(drone.exe
    drone.cpp drone.hpp
)
target_link_libraries(drone.exe
    MAVSDK::mavsdk
    MAVSDK::mavsdk_action
    MAVSDK::mavsdk_calibration
    MAVSDK::mavsdk_camera
    MAVSDK::mavsdk_failure
    MAVSDK::mavsdk_follow_me
    MAVSDK::mavsdk_ftp
    MAVSDK::mavsdk_geofence
    MAVSDK::mavsdk_gimbal
    MAVSDK::mavsdk_info
    MAVSDK::mavsdk_log_files
    MAVSDK::mavsdk_manual_control
    MAVSDK::mavsdk_mavlink_passthrough
    MAVSDK::mavsdk_mission_raw
    MAVSDK::mavsdk_mission
    MAVSDK::mavsdk_mocap
    MAVSDK::mavsdk_offboard
    MAVSDK::mavsdk_param
    MAVSDK::mavsdk_shell
    MAVSDK::mavsdk_telemetry
    MAVSDK::mavsdk_tune
)

# create library
add_library(drone SHARED
    drone.cpp drone.hpp
)
#target_link_libraries(drone
#    MAVSDK::mavsdk_telemetry
#    MAVSDK::mavsdk_action
#    MAVSDK::mavsdk_camera
#    MAVSDK::mavsdk_mission
#    MAVSDK::mavsdk_gimbal
#    MAVSDK::mavsdk
#)


target_link_libraries(drone
    MAVSDK::mavsdk_action
    MAVSDK::mavsdk_calibration
    MAVSDK::mavsdk_camera
    MAVSDK::mavsdk_failure
    MAVSDK::mavsdk_follow_me
    MAVSDK::mavsdk_ftp
    MAVSDK::mavsdk_geofence
    MAVSDK::mavsdk_gimbal
    MAVSDK::mavsdk_info
    MAVSDK::mavsdk_log_files
    MAVSDK::mavsdk_manual_control
    MAVSDK::mavsdk_mavlink_passthrough
    MAVSDK::mavsdk_mission_raw
    MAVSDK::mavsdk_mission
    MAVSDK::mavsdk_mocap
    MAVSDK::mavsdk_offboard
    MAVSDK::mavsdk_param
    MAVSDK::mavsdk_shell
    MAVSDK::mavsdk
    MAVSDK::mavsdk_telemetry
    MAVSDK::mavsdk_tune
)
install(TARGETS drone)

# add documentation target
# auto build new documentation on Release build
if (CMAKE_BUILD_TYPE MATCHES "^[Rr]elease")
    option(BUILD_DOC "Build documentation" ON)
    find_package(Doxygen)
    if (DOXYGEN_FOUND)
        add_custom_target( Documentation ALL # always build
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
            COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile
            COMMENT "Generating API documentation with Doxygen"
            VERBATIM )
    else (DOXYGEN_FOUND)
      message("Doxygen need to be installed to generate the doxygen documentation")
    endif (DOXYGEN_FOUND)
# on any other build create target but don't run it
else ()
  option(BUILD_DOC "Build documentation" ON)
  find_package(Doxygen)
  if (DOXYGEN_FOUND)
      add_custom_target( Documentation # build on demand
          WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
          COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile
          COMMENT "Generating API documentation with Doxygen"
          VERBATIM )
  else (DOXYGEN_FOUND)
    message("Doxygen need to be installed to generate the doxygen documentation")
  endif (DOXYGEN_FOUND)
endif()